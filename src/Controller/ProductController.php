<?php

namespace App\Controller;
use App\Entity\Product;
use App\Controller\OutputController;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\AcceptHeader;
use Doctrine\ORM\ORMException;
use Doctrine\DBAL\DBALException;

class ProductController extends AbstractController
{
    /**
     * @Route("/api/product", name="product_create", methods = {"POST"})
     */
    public function create()
    {   
        try
        {
            $request = Request::createFromGlobals();
            
            //ENTITY MANAGER
            $entityManager = $this->getDoctrine()->getManager();
            $product = new Product();
            $product->setName($request->request->get('name'));
            $product->setPrice($request->request->get('price'));
            $product->setDescription($request->request->get('description'));            
            $entityManager->persist($product);
            $entityManager->flush();         
                        
            //GET PRODUCT
            $product = $this->getDoctrine()->getRepository(Product::class)->find($product->getId());            
            if (!$product) {
                throw $this->createNotFoundException('error');
            }
            
            //OUTPUT
            $response = new Response(json_encode($product->toArray()));            
            $response -> setStatusCode(Response::HTTP_OK);
        }                         

        catch (\Exception $e) {            
            $response = New Response(json_encode(array('error' => $e->getMessage())));            
            $response -> setStatusCode(Response::HTTP_BAD_REQUEST);
        }

        $response->headers->set('Content-Type', 'application/json');
        return $response;         
    } 
 
    
    /**
     * @Route("/api/product/{id}", name="product_read", methods = {"GET"})
     */
    public function read($id)
    {   
        try
        {
            //GET PRODUCT
            $repository = $this->getDoctrine()->getRepository(Product::class)->find($id);            
            $productId = $repository;
            $product = $this->getDoctrine()->getRepository(Product::class)->find($productId);            
            if (!$product) {
                throw $this->createNotFoundException('product not found');
            }
            
            //OUTPUT
            $response = new Response(json_encode($product->toArray()));            
            $response -> setStatusCode(Response::HTTP_OK);
        }                            
        catch (\Exception $e) {            
            $response = New Response(json_encode(array('error' => $e->getMessage())));            
            $response -> setStatusCode(Response::HTTP_BAD_REQUEST);
        }

        $response->headers->set('Content-Type', 'application/json');
        return $response;         
    } 

    /**
    * @Route("/api/product/update", name="product_udpate", methods = {"PUT"})
    */
    public function update()
    {   
        try
        {
            //GET PRODUCT
            $request = Request::createFromGlobals();                         
            $product = $this->getDoctrine()->getRepository(Product::class)->find($request->request->get('id'));            
            if (!$product) {
                throw $this->createNotFoundException('product not found');
            }
           
            //ENTITY MANAGER
            $entityManager = $this->getDoctrine()->getManager();            
            $product->setName($request->request->get('name'));
            $product->setPrice($request->request->get('price'));
            $product->setDescription($request->request->get('description'));            
            $entityManager->merge($product);
            $entityManager->flush();         
                                            
            //OUTPUT
            $response = new Response(json_encode($product->toArray()));            
            $response -> setStatusCode(Response::HTTP_OK);
        }     
        catch (\Exception $e) {            
            $response = New Response(json_encode(array('error' => $e->getMessage())));            
            $response -> setStatusCode(Response::HTTP_BAD_REQUEST);
        }

        $response->headers->set('Content-Type', 'application/json');
        return $response;  

    }


    /**
     * @Route("/api/product/delete/{id}", name="product_delete", methods = {"DELETE"})
     */
    public function delete($id)
    {   
        try
        {
            //GET PRODUCT
            $repository = $this->getDoctrine()->getRepository(Product::class);            
            $product = $repository->find($id);    
            if (!$product) {
                throw $this->createNotFoundException('product not found');
            }     
            
            //ENTITY MANAGER
            $entityManager = $this->getDoctrine()->getManager();           
            $entityManager->remove($product);
            $entityManager->flush();
            
            //OUTPUT
            $response = new Response(json_encode(array('result' => "product has been deleted")));            
            $response -> setStatusCode(Response::HTTP_OK);
            
        }            
        catch (\Exception $e) {            
            $response = New Response(json_encode(array('error' => $e->getMessage())));            
            $response -> setStatusCode(Response::HTTP_BAD_REQUEST);
        }
        $response->headers->set('Content-Type', 'application/json');
        return $response;         
    } 


    
    /**
     * @Route("/api/products", name="products", methods = {"GET"})
     */
    public function products()
    {   
        try
        {
            //GET PRODUCTS
            $repository = $this->getDoctrine()->getRepository(Product::class);            
            $products = $repository->findAll();                                              
            foreach ($products as $product) {
                $response[] = $product->toArray();
            }

            //OUTPUT
            $response = new Response(json_encode(array('result' => $response)));            
            $response -> setStatusCode(Response::HTTP_OK);
            
        }                                   
        catch (\Exception $e) {            
            $response = New Response(json_encode(array('error' => $e->getMessage())));            
            $response -> setStatusCode(Response::HTTP_BAD_REQUEST);
        }
        $response->headers->set('Content-Type', 'application/json');
        return $response;         
    } 

 
}



